# Parcours de vélo épicurien — GLO-4035-2019

Vous avez eu l’excellente idée pour une application! Il s’agit d’une application permettant à un utilisateur d’obtenir un parcours à vélo sur lequel sont situés les meilleurs restaurants d’une région. Afin de matérialiser votre idée, vous désirez monter un dossier de candidature pouvant être présenté à des investisseurs potentiels (dont la fondation Pierre-Ardouin) qui financeront son développement (en échange de votre âme).

Liste des [appels possibles](./app/README.md) !

![Exemple de parcours](./docs/example/example.png)
