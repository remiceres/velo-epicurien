\chapter{Présentation des données}

Pour réaliser ce projet, deux types de données sont nécessaires, les restaurants à visiter et les routes pouvant être empruntées à vélo.
Les données des restaurants doivent contenir au minimum les coordonnées géographiques, le type de cuisine servi et une note. Les données de routes comprennent non seulement les pistes cyclables mais aussi les autres types de voies praticables en vélo.
Cette application preuve de concept se concentre uniquement sur la ville de Montpellier en France.

\section{Sources de données}

Les données nécessaires au projet sont issues de la base de données géographique libre et contributive \textit{OpenStreetMap} (OSM). Cette base de données répond parfaitement à nos besoins. En effet, elle contient toutes les informations demandées sur les routes et les restaurants.
De plus, ces données sont distribuées sous licence libre \textit{ODC Open Database License} (ODbL)~\parencite{opsm:ODbl}. 
Enfin, puisque les données d’OSM couvrent l'ensemble de la Terre, il sera simple par la suite d'étendre la portée de l'application à d'autres ville du monde.


Les données ont été extraites à l'aide d'\textit{Overpass Turbo} \footnote{Site web d'\textit{Overpass Turbo} : \url{http://overpass-turbo.eu/}}. Ce service permet d'acquérir les données en simplifiant l'écriture de requêtes sur l'API \textit{Overpass} d’OSM.
La figure~\ref{fig:requete} est un exemple de requête permettant de récupérer les restaurants, les cafés et les \textit{fast food} de la ville de Montpellier. L'ensemble des requêtes réalisées pour ce projet sont disponibles dans le dossier du projet \href{https://gitlab.com/remiceres/velo-epicurien/tree/master/docs/import\%20data}{\texttt{vélo-epicurien/docs/import data}}.

\begin{figure}[htb!]
    \centering
    \begin{lstlisting}[language=json,firstnumber=1]
[out:json][timeout:25];{{geocodeArea:montpellier}}->.searchArea;
(
  node["amenity"="restaurant"](area.searchArea);
  node["amenity"="fast_food"](area.searchArea);
  node["amenity"="cafe"](area.searchArea);
);
out body;>;out skel qt;
    \end{lstlisting}
    \caption{\textbf{Requête \textit{Overpass Turbo}} retournant les lieux de restauration de Montpellier}
    \label{fig:requete}
\end{figure}


\section{Taille des données}

\label{sec:analyse-descriptive-données}
Les données extraites d’OSM pour la ville de Montpellier comprennent~848 restaurants pour une taille de 466~ko ainsi que 1\,196~km de routes demandant 13~Mo de stockage.

Malheureusement, les données des restaurants ne contiennent pas de notes et le type de cuisine n'est pas renseigné pour toutes les enseignes. Pour les besoins de cette preuve de concept, ces données ont été complétées en générant aléatoirement les valeurs manquantes à l'aide du script disponible dans le dossier  
\href{https://gitlab.com/remiceres/velo-epicurien/blob/master/docs/import\%20data/restaurants/transform_data.py}{\texttt{vélo-epicurien/docs/import data/restaurants/transform\_data.py}} .

\section{Analyse descriptive des données}

Les données des restaurants récupérées se présentent sous la forme d'un fichier~JSON. Chaque restaurant est décrit dans un objet similaire à l'exemple présenté en figure~\ref{fig:resto-data}. Les attributs les plus intéressants dans le cadre du projet sont~:
\begin{itemize}
    \item les coordonnées géographiques suivant le standard GeoJSON (clé \texttt{geometry})~;
    \item le nom du restaurant (clé \texttt{properties.name})~;
    \item la note du restaurant (clé \texttt{properties.cote})~;
    \item le type de cuisine servi (clé \texttt{properties.cuisine}).
\end{itemize}

\begin{figure}[h!]
    \centering
    \begin{lstlisting}[language=json,firstnumber=1]
{
    "geometry": {
        "coordinates": [3.9162494, 43.6036824],
        "type": "Point"
    },
    "id": "node/323130276",
    "properties": {
        "addr:city": "Montpellier",
        "addr:street": "Place de France",
        "amenity": "restaurant",
        "cote": "4.8",
        "cuisine": "french",
        "name": "Les 3 Brasseurs",
        "phone": "+33 4 67 20 14 48",
        "wheelchair": "yes",
    },
    "type": "Feature"
}
    \end{lstlisting}
    \caption{\textbf{Exemple de données pour un restaurant.} Ici, \enquote{Les 3 brasseurs}.}
    \label{fig:resto-data}
\end{figure}

\newpage

Les données des routes sont également récupérées dans un format~JSON.
Dans~OSM, chaque voie est constituée d’une succession de points reliés par des segments rectilignes. Un point peut être partagé par plusieurs voies, indiquant que les voies concernées se croisent et qu’il est possible de passer de l’une à l’autre.
À l’inverse, par exemple, une voie passant au dessous d’une autre au moyen d’un tunnel ne partagera pas de point d’intersection mais les deux voies comporteront simplement deux points distincts à la même position géographique.

Le fichier récupéré regroupe tous les points existants (type \enquote{node}) ainsi que les voies (type \enquote{way}) qui sont composées d’une succession de points.
Un point contient un \texttt{id} unique ainsi que ses coordonnées géographiques dans les champs \texttt{lat} et \texttt{lon}.
Une voie contient également un \texttt{id} unique, la liste des points qui la composent dans le champ \texttt{nodes} ainsi que des informations permettant notamment d'identifier sa catégorie de route (piste cyclable, route primaire, ...) dans le champ \texttt{tags}.
La figure~\ref{fig:route-data} donne un exemple de données de route.

\begin{figure}[ht!]
  \centering
  \begin{lstlisting}[language=json,firstnumber=1]
[
    {
        "type": "way",
        "id": 10154009,
        "nodes": [70954681, 2387982440, 70918617],
        "tags": {
            "highway": "cycleway",
            "segregated": "yes"
        }
    },
    {
        "type": "node",
        "id": 70954681,
        "lat": 43.6287934,
        "lon": 3.8833386
    },
    {
        "type": "node",
        "id": 2387982440,
        "lat": 43.628755,
        "lon": 3.8833738
    },
    {
        "type": "node",
        "id": 70918617,
        "lat": 43.6287218,
        "lon": 3.8833814
    }
]
  \end{lstlisting}
  \caption{\textbf{Exemple de données de route.}}
  \label{fig:route-data}
\end{figure}



