\documentclass{report}
\overfullrule=1mm


% Support d’Unicode et meilleure police
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{ulem}
\normalem

% Mathématiques
\usepackage{mathtools}
\usepackage{amssymb}

\DeclarePairedDelimiter\ceil{\lceil}{\rceil}
\DeclarePairedDelimiter\floor{\lfloor}{\rfloor}

% Marges du document
\usepackage{geometry}

% Francisations
\usepackage[french]{babel}
\usepackage{csquotes}

% Figures et tables
\usepackage[pdftex]{graphicx}
\usepackage{booktabs}
\usepackage{tabularx}
\usepackage{framed}

\newcolumntype{C}{>{\centering\arraybackslash}X}

% Graphismes
\usepackage{tikz}
\usepackage{pgfgantt}
\usepackage{lscape}
\usepackage{rotating}
\usepackage{float}
\usepackage{amsmath}
\usepackage{mathdots}
\usepackage{yhmath}
\usepackage{cancel}
\usepackage{color}
\usepackage{siunitx}
\usepackage{array}
\usepackage{multirow}
\usepackage{gensymb}
\usetikzlibrary{fadings}


\title{Parcours de vélo épicurien}
\author{Antoine Battagliotti,\\ Coline Bolland,\\ Rémi Cérès}
\date{\today}

% Énumérations améliorées
\usepackage{enumitem}

% Structure
\usepackage{minitoc}
\usepackage[toc,page]{appendix}

% Point médian
\usepackage{ifthen}
\newcommand{\Mme}{M\textsuperscript{me}}
\newcommand{\Mmes}{M\textsuperscript{mes}}
\newcommand{\ptm}[2]{{\fontfamily{cmr}\selectfont\textperiodcentered}#1\ifthenelse{\equal{#2}{}}{}{{\fontfamily{cmr}\selectfont\textperiodcentered}#2}}

% Sections et chapitres
\addto{\captionsfrench}{\renewcommand{\chaptername}{Partie}}

% Titres
\usepackage{titlesec}
\titleformat{\chapter}[display]
    {\normalfont\huge\bfseries}
    {\chaptertitlename~\thechapter}
    {0pt}{\Huge}

\titlespacing*{\chapter}{0pt}{0pt}{15pt}
\titlespacing*{name=\chapter,numberless}{0pt}{-30pt}{10pt}

% Coloration syntaxique
\usepackage{listings}
\usepackage{textcomp}
\usepackage{color}

\lstset{
    breaklines=true,
    numbers=left,
    numberstyle=\tiny\color{black!70},
    basicstyle=\ttfamily\footnotesize,
    keywordstyle=\color{blue}\ttfamily,
    stringstyle=\color{red}\ttfamily,
    commentstyle=\color{gray}\ttfamily,
    morecomment=[l][\color{magenta}]{\#},
    literate=
        {á}{{\'a}}1 {é}{{\'e}}1 {í}{{\'i}}1 {ó}{{\'o}}1 {ú}{{\'u}}1
        {Á}{{\'A}}1 {É}{{\'E}}1 {Í}{{\'I}}1 {Ó}{{\'O}}1 {Ú}{{\'U}}1
        {à}{{\`a}}1 {è}{{\`e}}1 {ì}{{\`i}}1 {ò}{{\`o}}1 {ù}{{\`u}}1
        {À}{{\`A}}1 {È}{{\'E}}1 {Ì}{{\`I}}1 {Ò}{{\`O}}1 {Ù}{{\`U}}1
        {ä}{{\"a}}1 {ë}{{\"e}}1 {ï}{{\"i}}1 {ö}{{\"o}}1 {ü}{{\"u}}1
        {Ä}{{\"A}}1 {Ë}{{\"E}}1 {Ï}{{\"I}}1 {Ö}{{\"O}}1 {Ü}{{\"U}}1
        {â}{{\^a}}1 {ê}{{\^e}}1 {î}{{\^i}}1 {ô}{{\^o}}1 {û}{{\^u}}1
        {Â}{{\^A}}1 {Ê}{{\^E}}1 {Î}{{\^I}}1 {Ô}{{\^O}}1 {Û}{{\^U}}1
        {œ}{{\oe}}1 {Œ}{{\OE}}1 {æ}{{\ae}}1 {Æ}{{\AE}}1 {ß}{{\ss}}1
        {ű}{{\H{u}}}1 {Ű}{{\H{U}}}1 {ő}{{\H{o}}}1 {Ő}{{\H{O}}}1
        {ç}{{\c c}}1 {Ç}{{\c C}}1 {ø}{{\o}}1 {å}{{\r a}}1 {Å}{{\r A}}1
        {€}{{\euro}}1 {£}{{\pounds}}1 {«}{{\guillemotleft}}1
        {»}{{\guillemotright}}1 {ñ}{{\~n}}1 {Ñ}{{\~N}}1 {’}{{'}}1
}


% Définitions/glossaires
\newcommand{\glossaire}[2]{\vspace{20pt}%
\noindent%
\begin{tikzpicture}
\node [text width=\textwidth - 23pt, draw, inner sep=10pt]
    (definitions) {\begin{description}#2\end{description}};
\node [fill=white, inner xsep=10pt, yshift=2.5pt] at (definitions.north)
    {\textbf{\large #1}};
\end{tikzpicture}
}

% En-têtes et pieds de pages personnalisés
\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhf{}
\lhead{Rémi \textsc{Cérès}, Coline \textsc{Bolland}, Antoine \textsc{Battagliotti}}
\makeatletter
\rhead{\@title}
\makeatother
\cfoot{\thepage}

% Algorithme
\usepackage[]{algorithm2e}

% Bibliographie
\usepackage[
    style=authortitle,
    citestyle=authoryear,
    maxbibnames=99,
    maxcitenames=5,
    backend=biber
]{biblatex}

\setlength\bibitemsep{1em}
\renewcommand*{\nameyeardelim}{\addcomma\addspace}
\addbibresource{references.bib}

\DefineBibliographyStrings{french}{%
    urlseen = {consulté le},
    techreport = {Rapport technique},
}

% Espacement de paragraphes
\setlength{\parskip}{0.4cm plus4mm minus3mm}

%Notations personnalisés
 \newcommand{\exemple}[1]{
 \begin{quote}
    \rule{\linewidth}{.5pt}
    \begin{center}
        #1
    \end{center}
    \rule{\linewidth}{.5pt}
\end{quote}
}


% Hyperliens
\makeatletter
\usepackage[
    pdfauthor={Rémi Cérès},
    pdftitle={\@title}
]{hyperref}
\makeatother

% Support json lstlisting
\usepackage{listings}
\usepackage{xcolor}

\colorlet{punct}{red!60!black}
\definecolor{background}{HTML}{EEEEEE}
\definecolor{delim}{RGB}{20,105,176}
\colorlet{numb}{magenta!60!black}

\lstdefinelanguage{json}{
    basicstyle=\normalfont\ttfamily,
    numbers=left,
    numberstyle=\scriptsize,
    stepnumber=1,
    numbersep=8pt,
    showstringspaces=false,
    breaklines=true,
    frame=lines,
    backgroundcolor=\color{background},
    literate=
     *{0}{{{\color{numb}0}}}{1}
      {1}{{{\color{numb}1}}}{1}
      {2}{{{\color{numb}2}}}{1}
      {3}{{{\color{numb}3}}}{1}
      {4}{{{\color{numb}4}}}{1}
      {5}{{{\color{numb}5}}}{1}
      {6}{{{\color{numb}6}}}{1}
      {7}{{{\color{numb}7}}}{1}
      {8}{{{\color{numb}8}}}{1}
      {9}{{{\color{numb}9}}}{1}
      {:}{{{\color{punct}{:}}}}{1}
      {,}{{{\color{punct}{,}}}}{1}
      {\{}{{{\color{delim}{\{}}}}{1}
      {\}}{{{\color{delim}{\}}}}}{1}
      {[}{{{\color{delim}{[}}}}{1}
      {]}{{{\color{delim}{]}}}}{1},
}

\begin{document}
% Mise en place de la sous-table des matières pour la partie 5
\mtcsettitle{minitoc}{Liste des appels disponibles}
\dominitoc

% Page de garde
\include{./parties/titlepage}

% Table des matières
\setcounter{tocdepth}{1}
\tableofcontents

% Parties
\include{./parties/Introduction}
\include{./parties/Source}
\include{./parties/Technologie}
\include{parties/AlgoParcours}
\include{parties/Documentation}
\include{parties/Expansion}
\include{parties/Conclusion}
%PARTIE algorithme
    %Expliquer que les données de open street map etait bien faite et que les routes etait tute deja relier. ducoup pas besoin de strategie pour relier des routes±

%PARTIES explication du plan d’expansion
    % parler de open street map et du fait que lon peut prendre dn'importe qulee ville
    % Voir ce que dit le prof dans le cours sur la fragmentation a time : 1h50
    % interaction module data
    % donné deja relié parfaitement

% Bibliographie
\cleardoublepage
\phantomsection
\addcontentsline{toc}{chapter}{Bibliographie}
%\printbibliography

% Annexes
%\begin{appendices}
%\include{./annexes/json_data}
%\end{appendices}

\end{document}
