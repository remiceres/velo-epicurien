#!/usr/bin/env python3
import json
import os

INPUT_FILE = '/export.json'
OUT_FILE = '/paths.json'

# open and read in the input file
with open(os.getcwd() + INPUT_FILE, 'r') as original_file:
    json_content = json.load(original_file)

# get only elements jey content
json_content = json_content['elements']

# open and write in the output file
with open(os.getcwd() + OUT_FILE, 'w') as new_file:
    json_content = json.dump(json_content, new_file, indent=4)
