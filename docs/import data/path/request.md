# Voix empruntable avec un vélo de la ville de Montpellier (France)

## Télécharger les données
- [Hotpoint](http://overpass-turbo.eu)
- [API](https://wiki.openstreetmap.org/wiki/Overpass_API)

```
[out:json];
// fetch area “montpellier” to search in
{{geocodeArea:montpellier}}->.searchArea;

// gather results
(
  way["highway"="cycleway"](area.searchArea);
  way["highway"="pedestrian"](area.searchArea);
  way["highway"="living_street"](area.searchArea);
  way["highway"="footway"](area.searchArea);
  way["highway"="path"](area.searchArea);
  way["bicycle"="yes"](area.searchArea);
  way["highway"="tertiary"](area.searchArea);
  
  way["highway"="primary"](area.searchArea);
  way["highway"="secondary"](area.searchArea);
  way["highway"="tertiary"](area.searchArea);
  way["highway"="unclassified"](area.searchArea);
  way["highway"="residential"](area.searchArea);
);
// print results
out body;
>;
out skel qt;
```

## Transformer les données

```sh
./transform_data.py
```

## Insérer les données
Sur l'[interface web de neo4j](http://localhost:7474/browser/) 
```
CREATE INDEX ON :Node(id);
```

```
CALL apoc.load.json("https://drive.matteodelabre.me/index.php/s/eC9d3EaminYYktD/download") YIELD value

// Création d’un nœud
FOREACH (_ IN CASE WHEN value.type = 'node' THEN [1] ELSE [] END |
    MERGE (:Node {
        id: value.id,
        position: point({
            longitude: value.lon,
            latitude: value.lat
        })
    })
)

// Pour les éléments reliant des nœuds, création des relations paires par paires
WITH [
    idx in range(0, coalesce(size(value.nodes) - 2, -1))
    | [value.nodes[idx], value.nodes[idx + 1]]
]
AS node_pairs
UNWIND node_pairs AS pair
    MATCH (left:Node {id: pair[0]}), (right:Node {id: pair[1]})
    MERGE (left) <-[r:WAY {
        distance: distance(left.position, right.position)
    }]-> (right);
```