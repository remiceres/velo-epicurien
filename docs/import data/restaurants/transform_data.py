#!/usr/bin/env python3
import json
import os
import random
from random import uniform

INPUT_FILE = '/export.geojson'
OUT_FILE = '/restaurants.geojson'

# open and read in the input file
with open(os.getcwd() + INPUT_FILE, 'r') as original_file:
    json_content = json.load(original_file)

# get only features jey content
json_content = json_content['features']


# build set of cuisine available
cuisine_available = set()
for element in json_content:

    if 'cuisine' in element['properties']:
        cuisines = element['properties']['cuisine'].split(';')

        for cuisine in cuisines:
            cuisine_available.add(cuisine)


# adds the cooking and cote field
for element in json_content:

    if 'cuisine' not in element['properties']:
        element['properties']['cuisine'] = (
            random.sample(cuisine_available, 1)
        )[0]

    element['properties']['cote'] = "%.1f" % uniform(2.5, 5)


# # open and write in the output file
with open(os.getcwd() + OUT_FILE, 'w') as new_file:
    json_content = json.dump(json_content, new_file, sort_keys=True, indent=4)
