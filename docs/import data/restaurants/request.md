# Resturants et restauration rapide de la ville de Montpellier (France)

## Télécharger les données
- [Hotpoint](http://overpass-turbo.eu)
- [API](https://wiki.openstreetmap.org/wiki/Overpass_API)

```
[out:json][timeout:25];
{{geocodeArea:montpellier}}->.searchArea;

// gather results
(
  node["amenity"="restaurant"](area.searchArea);
  node["amenity"="fast_food"](area.searchArea);
  node["amenity"="cafe"](area.searchArea);
);

// print results
out body;
>;
out skel qt;
```

## Transformer les données

```sh
./transform_data.py
```

## Insérer les données
Dans un terminal
```
docker exec -it mongo-velo bash
```

```
apt-get update -qq
```

```
apt-get -y install curl -qq
```

```
curl "https://drive.matteodelabre.me/index.php/s/7EQgn98WnEKy9KD/download" --output /data/restaurant.geojson
```

```
mongoimport --db velo --collection restaurants --file /data/restaurant.geojson --jsonArray --drop
```