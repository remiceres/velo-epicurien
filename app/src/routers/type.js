/** 
 * Express router providing type retaurants routes
 * @module routers/type
 * @requires express
 */

const { Router } = require('express');
const data = require('../data');

const router = Router();



/**
 * List type of restaurant available
 * @return {String[]}
 */
router.get('/type', async (req, res) =>
{
    res.json(await data.typeRestaurants());
});


/**
 * Top restaurant type by number
 */
router.get('/topType', async (req, res) => 
{
    res.json(await data.topTypeRestaurants());
});



module.exports = router;