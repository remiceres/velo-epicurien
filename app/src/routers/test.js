/** 
 * Express router providing test related routes
 * @module routers/test
 * @requires express
 */

const {Router} = require('express');
const data = require('../data');

const router = Router();



/**
 * Check if server works
 * @name get/test
 */
router.get('/', (req, res) => 
{
    res.end("it's working :) !!");
});


/**
 * Check if the mongo server works
 * @name get/mongo 
 * 
 */
router.get('/mongo', async (req, res) =>
{
    const nbResto = await data.nbRestaurants();
    res.json(nbResto);
});


/**
 * Check if the neo4j server works
 * @name get/neo4j
 */
router.get('/neo4j', async (req, res) =>
{
    const totalPathLength = await data.totalPathLength();
    res.json(totalPathLength);
});



module.exports = router;