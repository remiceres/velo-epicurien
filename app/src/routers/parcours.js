/**
 * Express router providing route routes
 * @module routers/parcours
 * @requires express
 */

const { Router } = require('express');
const data = require('../data');

const router = Router();



/**
 * Generate a parcours
 * @param {Object} req - request object
 * @return {Object} parcours or error
 */
const generateRoute = async (req) =>
{
    // Check if request body is well formated
    let startingPoint = undefined;
    let maximumLength = undefined;
    let numberOfStops = undefined;
    let type = undefined;

    if (req.body !== undefined
        && req.body.starting_point !== undefined
        && req.body.maximum_length !== undefined
        && req.body.number_of_stops !== undefined
        && req.body.type !== undefined) 
    {
        startingPoint = req.body.starting_point;
        maximumLength = req.body.maximum_length;
        numberOfStops = req.body.number_of_stops;
        type = req.body.type;
    }


    if (
        startingPoint === undefined
        || maximumLength === undefined
        || numberOfStops === undefined
        || type === undefined)
    {
        console.log('Error (parcours) : malformed body');
        return {
            'type': 'Error',
            'message': 'malformed body',
            'body_example': {
                'starting_point': {
                    'type': 'Point',
                    'coordinates': [3.8803601, 43.6139025]
                },
                'maximum_length': 6000,
                'number_of_stops': 4,
                'type': ['chinese', 'asian']
            }
        };
    }


    // Build parcours
    let travelledDistance = 0;
    let stopMade = 0;
    const visitedRestaurants = [];

    let pathStartPoint = startingPoint;

    let segmentId = 1;
    const result = [];

    while (travelledDistance < maximumLength && stopMade < numberOfStops) 
    {
        // get next restaurant
        const {
            id: targetRestoId,
            restoInfo: restaurant,
            coordonate: pathEndPoint
        } = await data.nearestRestaurant(
            pathStartPoint,
            type,
            visitedRestaurants
        );

        // build path
        const { path, distance } = await data.pathBetween(
            pathStartPoint,
            pathEndPoint
        );

        const segmentJson = {};
        segmentJson['segment_id'] = segmentId;
        segmentJson['path'] = path;
        segmentJson['restaurant'] = restaurant;
        result.push(segmentJson);

        pathStartPoint = pathEndPoint;
        visitedRestaurants.push(targetRestoId);
        travelledDistance += distance;
        stopMade++;
        segmentId++;
    }

    console.log('visitedRestaurants: ', visitedRestaurants);
    console.log('travelledDistance: ', travelledDistance);
    return result;
};



/**
 * Generate a route in geoJson
 * @param {Object} body
 * @param {Object} body.starting_point - point in geoJson format
 * @param {number} body.maximum_length - maximum length of route
 * @param {number} body.number_of_stops - number of strop in route
 * @param {string[]} body.type - type of restaurant available
 * @return {Object[]} res - list of segments
 * @return {int} res.segment_id
 * @return {Object} res.path - path in geojson format
 * @return {Object} res.restaurant - description of restaurant
 */
router.get('/parcours/geojson', async (req, res) =>
{
    const parcours = await generateRoute(req);
    
    if ('type' in parcours && parcours.type === 'Error')
    {
        res.status(400);
    }

    const startPoint = {
        type: 'Feature',
        geometry: req.body.starting_point,
        properties: {
            name: 'Start'
        },
    };

    const restaurants = parcours.map(x => 
    {
        return {
            type: 'Feature',
            geometry: {
                type: 'Point',
                coordinates: x.path.coordinates[x.path.coordinates.length - 1],
            },
            properties: {
                name: x.segment_id + ' — ' + x.restaurant.name,
                cote: x.restaurant.cote,
                type: x.restaurant.type,
            },
        };
    });

    const path = parcours.map(({ path: { coordinates } }) => ({
        type: 'Feature',
        properties: {},
        geometry: coordinates.length === 1 ? {
            type: 'Point',
            coordinates: coordinates[0]
        } : {
            type: 'LineString',
            coordinates
        }
    }));

    const result = {
        type: 'FeatureCollection',
        features: [].concat(startPoint, restaurants, path)
    };

    res.json(result);
});


/**
 * Generate a route in prof format
 * @param {Object} body
 * @param {Object} body.starting_point - point in geoJson format
 * @param {number} body.maximum_length - maximum length of route
 * @param {number} body.number_of_stops - number of strop in route
 * @param {string[]} body.type - type of restaurant available
 * @return {Object[]} res - list of segments
 * @return {int} res.segment_id
 * @return {Object} res.path - path in geojson format
 * @return {Object} res.restaurant - description of restaurant
 */
router.get('/parcours', async (req, res) => 
{
    const parcours = await generateRoute(req);

    if ('type' in parcours && parcours.type === 'Error') 
    {
        res.status(400);
    }

    res.json(parcours);
});

module.exports = router;