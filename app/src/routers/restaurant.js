/**
 * Express router providing restaurant routes
 * @module routers/restaurant
 * @requires express
 */

const { Router } = require('express');
const data = require('../data');

const router = Router();

/**
 * Generate a route in geoJson
 * @param {Object} body
 * @param {Object} body.position - point in geoJson format
 * @param {string[]} body.type - type of restaurant available
 * @return {Object[]} res - list restaurants
 * @return {int} res.id - restaurant id 
 * @return {Object} res.restoInfo - restaurants informations 
 * @return {Object} res.coordonate - restaurants coordonates
 */
router.get('/near', async (req, res) => 
{
    
    // check parameters
    // Check if request body is well formated
    let position = undefined;
    let type = undefined;

    if (req.body !== undefined
        && req.body.position !== undefined
        && req.body.type !== undefined) 
    {
        position = req.body.position;
        type = req.body.type;
    }

    if (position === undefined
        || type === undefined) 
    {
        console.log('Error (restaurants/near) : malformed body');
        return {
            'type': 'Error',
            'message': 'malformed body',
            'body_example': {
                'position': {
                    'type': 'Point',
                    'coordinates': [
                        3.8803601,
                        43.6139025
                    ]
                },
                'type': [
                    'chinese',
                    'asian'
                ]
            }
        };
    }

    const result = await data.nearestRestaurant(
        position,
        type
    );

    res.json(result);
});

module.exports = router;
