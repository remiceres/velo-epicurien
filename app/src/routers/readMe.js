/**
 * Express router providing readMe routes
 * @module routers/readMe
 * @requires express
 */

const { Router } = require('express');
const path = require('path');

const router = Router();

/** Get content of the  readme.md file
  * @return readme file
**/
router.get('/readme', async (req, res) => 
{
    res.type('text/plain; charset=UTF-8');
    res.sendFile('README.md', { root: path.join(__dirname, '../..') });
});

module.exports = router;