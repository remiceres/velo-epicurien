/**
 * Express router providing starting point routes
 * @module routers/startingPoint
 * @requires express
 */

const { Router } = require('express');
const data = require('../data');

const router = Router();



/**
  * Get a starting point
  * @param {Object} body
  * @param {Object} body.starting_point - starting point in geoJson format
  * @param {String} body.type - describle object type
  * @param {number[]} body.coordinates
  * @return {Object} res
  * @return {Object} res.starting_point - point in geojson format
  */
router.get('/starting-point', async (req, res) =>
{
    // Check if request body is well formated
    let type = undefined;
    if (req.body != undefined && req.body.type != undefined)
    {
        type = req.body.type;
    }

    if (type === undefined)
    {
        console.log('Error (startingPoint) : malformed body');
        res.status(400);
        res.json(
            {
                'type': 'Error',
                'message': 'malformed body: type must not be empty',
                'body_example':
                {
                    'maximum_length': 1000,
                    'type': ['chinese', 'asian']
                }
            }
        );
        return;
    }

    const result = await data.randomRestaurantCoordinates(type);
    res.json({ 'starting_point': result});
});


module.exports = router;