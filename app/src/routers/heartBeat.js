/**
 * Express router providing heartbeat routes
 * @module routers/heartBeat
 * @requires express
 */

const {Router} = require('express');
const data = require('../data');

const router = Router();



/**
 * Compute informations about restaurants and paths
 * @return {Object}
 * @return {number} Object.nb_restaurants
 * @return {number} Object.total_path_length
 */
router.get('/heartbeat', async (req, res) => 
{
    const result = {};

    result['nb_restaurants'] = await data.nbRestaurants();
    result['total_path_length'] = await data.totalPathLength();

    res.json(result);
});



module.exports = router;