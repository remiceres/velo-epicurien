/** 
 * Exchange functions with MongoDB database
 * @module data/mongodb
 * @requires mongodb
 */

const { MongoClient, MongoTimeoutError } = require('mongodb');

const IP_SERVER = (process.env['PRODUCTION'] === '1') ? 'mongo' : 'localhost';
const URL_SERVER = `mongodb://${IP_SERVER}:27017/velo`;


/**
 * Connect to MongoDB server with retry
 * 
 * @param {number} [nbRetry = 10] - Number of retry
 */
const connectMongoRetry = async (nbRetry = 10) => 
{
    try 
    {
        return (await MongoClient.connect(URL_SERVER, {
            useUnifiedTopology: true,
            socketTimeoutMS: 10E5
        })).db('velo');
    }
    catch (err) 
    {
        if (err instanceof MongoTimeoutError && nbRetry > 0) 
        {
            console.warn('MongoDB database connection timed out. Retrying.');
            return await connectMongoRetry(nbRetry - 1);
        }

        throw err;
    }
};

const db = (async () => await connectMongoRetry())();


/**
 * Build a geoSpatial index on 'geometry' field
 */
(async () => await (await db)
    .collection('restaurants')
    .createIndex({ geometry: '2dsphere' })
)();


/**
 * Count number of restaurants
 * @return {number}
 */
const nbRestaurants = async () => 
{
    return await (await db).collection('restaurants').find().count();
};


/**
 * List type of restaurant available
 * @return {String[]}
 */
const typeRestaurants = async () => 
{
    const result = await (await db).collection('restaurants').aggregate(
        [
            {
                $project:
                {
                    name: '$properties.name',
                    typeCuisine: { $split: ['$properties.cuisine', ';'] }
                }
            },
            { $unwind: '$typeCuisine' },
            { $group: { _id: '$typeCuisine' } },
            { $sort: { _id: 1 } },
        ]
    );

    return (await result.toArray()).map(el => el._id);
};


/**
 * Top restaurant type by number
 * @return {Object[]}
 * @return Object._id - Type of restaurant
 * @return Object.nb - Number of restaurant with this type
 */
const topTypeRestaurants = async () => 
{
    const result = await (await db).collection('restaurants').aggregate(
        [
            {
                $project:
                {
                    name: '$properties.name',
                    typeCuisine: { $split: ['$properties.cuisine', ';'] }
                }
            },
            { $unwind: '$typeCuisine' },
            { $group: { _id: '$typeCuisine', nb: { $sum: 1 } } },
            { $sort: { nb: -1 } },
        ]
    );

    return result.toArray();
};


/**
 * Get a ramdomly coordonate near random restaurant among some type
 * @param {String[]} type - list of restaurant type enable
 * @param {String[]} [exclude] - list of restaurant exclude in research
 * @return {Object} point in format geoJson
 */
const randomRestaurantCoordinates = async (type, exclude = []) => 
{
    // check parameter
    if (type === undefined)
    {
        throw 'Error (mongodb/randomRestaurantCoordinates) :'
            + 'type must not be empty';
    }

    const query = {
        'properties.cuisine': { $in: type },
        'id': { $nin: exclude },
    };

    // select an random restaurant
    const nbResult = await (await db)
        .collection('restaurants')
        .countDocuments(query);
    const randomI = Math.floor(Math.random() * nbResult);

    const coordinates = await (await db)
        .collection('restaurants')
        .find(query)
        .project({ _id: 0, 'geometry.coordinates': 1})
        .limit(1)
        .skip(randomI)
        .toArray();

    let latitude = coordinates[0].geometry.coordinates[1];
    let longitude = coordinates[0].geometry.coordinates[0];
    
    // Apply random coordinates around the restaurant
    // 1km = 0.008 degre
    const randomSizeKm = 2;
    const randomSizeDegree = randomSizeKm * 0.008;
    latitude = Number(
        (latitude + (Math.random() * randomSizeDegree)).toFixed(7)
    );
    longitude = Number(
        (longitude + (Math.random() * randomSizeDegree)).toFixed(7)
    );

    // build geojson respons object
    const result = {
        type: 'Point',
        coordinates: [longitude, latitude],
    };

    return result;
};



/**
 * Get nearest restaurant with available type
 * @param {Object} position - starting point of search, point in geojson format
 * @param {String[]} type - list of restaurant type enable
 * @param {String[]} [exclude] - list of restaurant exclude in research
 * @return {Object[]} res - list restaurants
 * @return {int} res.id - restaurant id
 * @return {Object} res.restoInfo - restaurants informations
 * @return {Object} res.coordonate - restaurants coordonates
 */
const nearestRestaurant = async (position, type, exclude = []) =>
{
    // check parameters
    if (position === undefined || type === undefined) 
    {
        throw 'Error (mongodb/nearestRestaurant) :'
        + 'position or type must not be empty';
    }


    let res = await (await db).collection('restaurants').find(
        {
            'properties.cuisine': { $in: type },
            'id': { $nin: exclude },
            'geometry':
            {
                $near: {
                    $geometry: position
                }
            }
        },
    ).project(
        {
            _id: 0, 'geometry.coordinates': 1,
            'id': 1,
            'properties.name' :1,
            'properties.cuisine': 1,
            'properties.cote': 1,
        }
    ).toArray();

    
    const latitude = res[0].geometry.coordinates[1];
    const longitude = res[0].geometry.coordinates[0];
    const id = res[0].id;
    const restoInfo = { 
        'name': res[0].properties.name,
        'type': res[0].properties.cuisine,
        'cote': res[0].properties.cote,
    };

    const coordonate = {
        type: 'Point',
        coordinates: [longitude, latitude],
    };
    
    // TODO : faire fonction distance + intelligente que plus proche

    return { id, restoInfo, coordonate};
};





exports.nbRestaurants = nbRestaurants;
exports.typeRestaurants = typeRestaurants;
exports.topTypeRestaurants = topTypeRestaurants;
exports.randomRestaurantCoordinates = randomRestaurantCoordinates;
exports.nearestRestaurant = nearestRestaurant;