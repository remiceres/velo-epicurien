/**
 * Exchange functions with Neo4j database
 * @module data/neo4j
 * @requires mongodb
 */

const neo4j = require('neo4j-driver').v1;


const USER = 'neo4j';
const PSWD = 'enigma';

const IP_SERVER = (process.env['PRODUCTION'] === '1') ? 'neo4j' : 'localhost';
const URL_SERVER = `bolt://${IP_SERVER}`;

const driver = neo4j.driver(URL_SERVER, neo4j.auth.basic(USER, PSWD));
const session = driver.session();


/**
 * Top restaurant type by number
 * @return {number}
 */
const totalPathLength = async () =>
{
    const result = await session.run(`
        MATCH (left)-[way:WAY]->(right)
        RETURN sum(way.distance)
    `);

    return result.records[0].get(0);
};


/**
 * Get nearest road from coordonate
 * @param {Object} geoPoint - Point in geojson format
 * @return {Object} Coordinate of nearest road in geojson format
 */
const nearestRoadCoordinate = async (geoPoint) => 
{
    if (geoPoint === undefined) 
    {
        throw 'Error (neo4j/nearestRoadCoordinate) : ' 
            + 'geoPoint must not be empty';
    }

    let response = await session.run(`
        MATCH (p:Node)
        RETURN p
        ORDER BY distance(
            p.position,
            point({
                longitude:` + geoPoint.coordinates[0]
                + ', latitude:' + geoPoint.coordinates[1]
                + `}))
                LIMIT 1
    `);
    response = response.records[0].get(0).properties.position;
    
    // build geojson respons object
    const result = {};
    result['type'] = 'Point';
    result['coordinates'] = [response.x, response.y];
    
    return result;
};


/**
 * Get path between two points
 * @param {Object} start - start point of path in geojson format
 * @param {Object} end - end point of path in geojson format
 * @return {Object} path in geojson format
 */
const pathBetween = async (start, end) =>
{
    if (start === undefined || end === undefined) 
    {
        throw 'Error (neo4j/pathBetween) : start/end point must not be empty';
    }
    
    start = await nearestRoadCoordinate(start);
    end = await nearestRoadCoordinate(end);

    const request =
        'MATCH (start:Node), (end:Node)'
        + 'WHERE' 
            + ' start.position.x=' + start.coordinates[0]
            + ' AND start.position.y=' + start.coordinates[1]
            + ' AND end.position.x=' + end.coordinates[0]
            + ' AND end.position.y=' + end.coordinates[1]
        + ' CALL apoc.algo.dijkstra(start,end,"WAY","distance")'
        + ' YIELD path, weight'
        + ' RETURN path, weight';

    // transform path response to geojson format
    let coordinates = (
        await session.run(request)
    ).records[0]._fields[0].segments;
    
    coordinates = coordinates.map(x =>
    {
        const position = x.start.properties.position;
        return [position.x, position.y];
    });
    coordinates.push(end.coordinates);

    const path = {};
    path['type'] = 'LineString';
    path['coordinates'] = coordinates;

    // build distance response
    const distance = (await session.run(request)).records[0]._fields[1];


    return {path, distance};
};

exports.totalPathLength = totalPathLength;
exports.nearestRoadCoordinate = nearestRoadCoordinate;
exports.pathBetween = pathBetween;

