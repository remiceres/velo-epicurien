/**
 * Exchange functions with database
 * @module data/mongodb
 * @requires mongodb
 */

const mongoDB = require('./mongodb');
const neo4j = require('./neo4j');

module.exports = {...mongoDB, ...neo4j};