const morgan = require('morgan');
const express = require('express');

const test = require('./routers/test');
const heartbeat = require('./routers/heartBeat');
const type = require('./routers/type');
const startedPoint = require('./routers/startingPoint');
const parcours = require('./routers/parcours');
const readme = require('./routers/readMe');
const restaurant = require('./routers/restaurant');

const port = 8080;

// express configuration
const app = express();
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(morgan('tiny'));

// router configuration
app.use('/test', test);
app.use('/', heartbeat);
app.use('/', type);
app.use('/', startedPoint);
app.use('/', parcours);
app.use('/', readme);
app.use('/restaurant', restaurant);

app.listen(port, () => console.log(
    'Server running... < http://localhost:8080/heartbeat >'
));



// TODO tester résistance au panne not a JSON in body