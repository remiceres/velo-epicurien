# Parcours de vélo épicurien — GLO-4035-2019

Ce fichier contient tous les appels possibles, les payloads attendus et leurs réponses.

## 1 - Obtenir le README

**Appel** : `@GET` `/readme`

**Payload** : aucun payload est attendu

**Reponse** :
```md
# Parcours de vélo épicurien — GLO-4035-2019

Ce fichier contient tous les appels possibles, les payloads attendus et leurs réponses.
[...]
```

## Obtenir la liste des types de restaurants possibles

**Appel** : `@GET` `/type`

**Payload** : aucun payload est attendu

**Reponse**:

```json
[
    "african",
    "american",
    "argentinian",
    "armenian",
    [...]
]
```

## Obtenir le classement du nombre de types disponibles

**Appel** : `@GET` `/toptype`

**Payload** : aucun payload est attendu

**Reponse**:

```json
[
    {
        "_id": "pizza",
        "nb": 79
    },
    {
        "_id": "french",
        "nb": 65
    },
    {
        "_id": "kebab",
        "nb": 47
    },
    [...]
```

## Obtenir un point de départ

**Appel** : `@GET` `/starting-point`

**Payload** :

```json
{
    "maximum_length": 1000,
    "type": ["chinese", "asian", "tapas"]
}
```

**Reponse**:

```json
{
    "starting_point": {
        "type": "Point",
        "coordinates": [
            3.8923246,
            43.6240316
        ]
    }
}
```

## Obtenir un parcours

**Appel** : `@GET` `/parcours`

**Payload** :

```json
{
    "starting_point": {
        "type": "Point",
        "coordinates": [
            3.8803601,
            43.6139025
        ]
    },
    "maximum_length": 6000,
    "number_of_stops": 4,
    "type": [
        "pizza",
        "asian"
    ]
}
```

**Reponse** :

```json
[
    {
        "segment_id": 1,
        "path": {
            "type": "LineString",
            "coordinates": [
                [
                    3.8803623,
                    43.6138555
                ],
                [...]
            ]
        },
        "restaurant": {
            "name": "Lady Sushi",
            "type": "asian",
            "cote": "3.6"
        }
    },
    [...]
]
```

## Obtenir un parcours au format geoJson

**Appel** : `@GET` `/parcours/geojson`

**Payload** :

```json
{
    "starting_point": {
        "type": "Point",
        "coordinates": [
            3.8803601,
            43.6139025
        ]
    },
    "maximum_length": 6000,
    "number_of_stops": 4,
    "type": [
        "pizza",
        "asian"
    ]
}
```

**Reponse**:
```json
{
    "type": "FeatureCollection",
    "features": [
        {
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": [
                    3.881602,
                    43.6167064
                ]
            },
            "properties": {
                "name": "Start"
            }
        },
        [...]
    ]
}
```

## Obtenir le nombre de restaurants et la longueur des routes

**Appel** : `@GET` `/heartbeat`

**Payload** : aucun payload est attendu

**Reponse**:
```json
{
    "nb_restaurants": 848,
    "total_path_length": 1196891.6062111116
}
```

## Tester le fonctionnement du serveur
**Appel** : `@GET` `/test`

**Payload** : aucun payload est attendu

**Reponse**:
```
it's working :) !!
```

## Tester le fonctionnement de la connexion à la base de données Mongo
**Appel** : `@GET` `/test/mongo`

**Payload** : aucun payload est attendu

**Reponse**:
```
848
```

## Tester le fonctionnement de la connexion à la base de données neo4j
**Appel** : `@GET` `/test/neo4j`

**Payload** : aucun payload est attendu

**Reponse**:
```
1196891.6062111116
```

## Obtenir le restaurant le plus proche d'une coordonnée
**Appel** : `@GET` `/restaurant/near`

**Payload** : 
```json
{
    "position": {
        "type": "Point",
        "coordinates": [
            3.881602,
            43.6167064
        ]
    },
    "type": [
        "pizza"
    ]
}
```

**Reponse**:
```json
{
    "id": "node/1601102593",
    "restoInfo": {
        "name": "Pizzart",
        "type": "pizza",
        "cote": "3.2"
    },
    "coordonate": {
        "type": "Point",
        "coordinates": [
            3.8791497,
            43.6159585
        ]
    }
}
```
